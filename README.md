### Soluci�n en C# con POO

POOConsoleApp es una aplicaci�n de consola desarrollada en C# que ilustra los fundamentos de la programaci�n orientada a objetos (POO). Este proyecto est� compuesto por tres clases principales: Empleado, Departamento, y Program. Cada una de estas clases cumple un papel espec�fico, permitiendo entender y aplicar conceptos clave de la POO, como propiedades, m�todos, constructores y destructores.

#### Clases Principales

1. **Empleado:**
   - Atributos: `Nombre`, `Edad`, `Salario`.
   - M�todos: `MostrarInformacion`.
   - Constructor: Inicializa atributos y muestra mensaje al crear.
   - Destructor: Muestra mensaje al destruir.

2. **Departamento:**
   - Atributo: `empleados` (lista de empleados).
   - M�todos: `AgregarEmpleado`, `MostrarInformacionEmpleados`.
   - `AgregarEmpleado`: Agrega empleado y muestra mensaje.
   - `MostrarInformacionEmpleados`: Muestra info de empleados.

3. **Program:**
   - Funci�n Main: Crea empleados, un departamento y realiza operaciones.

#### Uso de POO

- **Propiedades autom�ticas:** Utilizadas en `Empleado`.
- **M�todos:** Definidos para mostrar informaci�n y operaciones espec�ficas.
- **Constructores:** Inicializan objetos de `Empleado`.
- **Destructores:** Muestran mensajes al destruir instancias de `Empleado`.

#### Ejemplo de Uso

```csharp
static void Main()
{
    Empleado empleado1 = new Empleado("Juan", 30, 50000);
    Empleado empleado2 = new Empleado("Mar�a", 25, 45000);

    Departamento departamento = new Departamento();

    departamento.AgregarEmpleado(empleado1);
    departamento.AgregarEmpleado(empleado2);

    departamento.MostrarInformacionEmpleados();
}
