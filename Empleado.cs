﻿using System;

namespace POOConsoleApp
{
    class Empleado
    {
        public string Nombre { get; set; }
        public int Edad { get; set; }
        public double Salario { get; set; }

        public Empleado(string nombre, int edad, double salario)
        {
            Nombre = nombre;
            Edad = edad;
            Salario = salario;
            Console.WriteLine($"Se ha creado un nuevo empleado: {Nombre}");
        }

        public void MostrarInformacion()
        {
            Console.WriteLine($"Nombre: {Nombre}, Edad: {Edad}, Salario: {Salario:C}");
        }

        ~Empleado()
        {
            Console.WriteLine($"Se ha destruido el empleado {Nombre}");
        }
    }
}
