﻿namespace POOConsoleApp
{
    internal class Program
    {
        static void Main()
        {
            Empleado empleado1 = new Empleado("Juan", 30, 50000);
            Empleado empleado2 = new Empleado("María", 25, 45000);

            Departamento departamento = new Departamento();

            departamento.AgregarEmpleado(empleado1);
            departamento.AgregarEmpleado(empleado2);

            departamento.MostrarInformacionEmpleados();

        }
    }
}
