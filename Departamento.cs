﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace POOConsoleApp
{
    class Departamento
    {
        private List<Empleado> empleados = new List<Empleado>();

        public void AgregarEmpleado(Empleado empleado)
        {
            empleados.Add(empleado);
            Console.WriteLine($"\nSe ha agregado a {empleado.Nombre} al departamento.");
        }

        public void MostrarInformacionEmpleados()
        {
            Console.WriteLine("\nInformación de empleados en el departamento:");
            foreach (Empleado empleado in empleados)
            {
                empleado.MostrarInformacion();
            }
        }
    }
}
